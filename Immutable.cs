﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FucProgram
{
    public class Immutable
    {
        /// <summary>
        /// read only properties
        /// </summary>
        int Length { get; }
        int Width { get; }

        public Immutable(int length, int width)
        {
            Length = length;
            Width = width;
        }

        /// <summary>
        /// crate a new instance
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public Immutable Grow(int x, int y) => new Immutable(Length + x, Width + y);
        public string Print => $"Length={Length}; Width={Width}";
    }
}
