﻿using System;
using System.Text;

namespace FucProgram
{
    class Program
    {
        static void Main(string[] args)
        {
            Immutable im = new Immutable(10, 15);
            Immutable getim = im.Grow(2, 3);

            Console.WriteLine(im.Print);
            Console.WriteLine(getim.Print);
            Console.WriteLine("im = getim: {0}", Object.ReferenceEquals(im, getim));

            bool[] bools = { false, false, true, false };

            // which way is better?
            Array.ForEach(bools.GetType().GetInterfaces(), item => {
                Console.WriteLine(item.ToString());
            });

            bools.GetType().GetInterfaces().ForEachPrint<Type>();

            Console.WriteLine(bools.CountType(bln => bln == false).ToString());
            Console.WriteLine(bools.CountType(bln => bln == true).ToString());

            Console.WriteLine(FucDelegate.StringWhen("Hello World", true));


            var getstring = new StringBuilder()
                .Append("<button")
                .AppendWhen(" disabled", true)
                .Append(">Click me</button>")
                .ToString();

            Console.WriteLine(getstring);

            int[] arrayInt = { 3, 4, 5, 2, 3, 7, 8 };
            var result = WithYeild.GreaterThan(arrayInt, 5);
            result.ForEachPrint<int>();

            var pos = valueTuple.GetCoordinates("11718 23 AVE, Edmonton");
            Console.WriteLine($"lat: {pos.lat.ToString()}; lng: {pos.lng.ToString()}");

            Console.ReadLine();
        }
    }
}
