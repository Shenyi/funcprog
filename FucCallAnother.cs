﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace FucProgram
{
    //Conventionally, however, either T or a name beginning with T is used.In this cases, it's indicating the type of the "source" element of the union. 
    //LINQ methods typically use the following type parameter names:
    //TSource: element type of the input (source)
    //TResult: element type of the output (result)
    //TKey: element type of a key used for things like grouping
    //TElement: element type of an intermediate sequence - this is more rarely used, but it appears in some overloads of GroupBy and similar methods
    public static class FucCallAnother
    {
        //https://docs.microsoft.com/en-us/dotnet/api/system.linq.enumerable.count?view=netframework-4.8#System_Linq_Enumerable_Count__1_System_Collections_Generic_IEnumerable___0__System_Func___0_System_Boolean
        //int IEnumerable.Count<T>(Func<T, Boolean> predicate)
        public static int CountType<TSource>(this IEnumerable<TSource> source, Func<TSource, Boolean> predicate)
        {
            int count = 0;
            foreach (TSource element in source)
            {
                checked
                {
                    if (predicate(element))
                    {
                        count++;
                    }
                }
            }
            return count;
        }

        public static void ForEachPrint<T>(this IEnumerable<T> source)
        {
            foreach (T element in source)
            {
                if (element is Type || element is int)
                    Console.WriteLine(element.ToString());
            }
        }
    }
}
