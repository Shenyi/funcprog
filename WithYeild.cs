﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FucProgram
{
    public class WithYeild
    {
        //Class hold the state instead of function
        public static IEnumerable<int> GreaterThan(int[] arr, int gt)
        {
            foreach(int n in arr)
            {
                if(n > gt) yield return n;
            }
        }
    }
}
