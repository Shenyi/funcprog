﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FucProgram
{
    public static class ShenyiStringBuilder
    {
        /// <summary>
        /// this System.Text.StringBuilder sb  refer to the "System.Text.StringBuilder"
        /// </summary>
        /// <param name="sb"></param>
        /// <param name="value"></param>
        /// <param name="predict"></param>
        /// <returns></returns>
        public static StringBuilder AppendWhen(this StringBuilder sb, string value, bool predict)
        => predict ? sb.Append(value) : sb;
    }
}
