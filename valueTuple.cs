﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FucProgram
{
    public class valueTuple
    {
        public static (double lat, double lng) GetCoordinates(string query)
        {
            //do search
            return (lat: 47.6457, lng: 122.1367);
        }


        public static Position GetPosition(string query)
        {
            //do search
            return new Position(47.6457,122.1367);
        }
    }

    public class Position
    {
        public Position(double lat, double lng)
        {
            this.lat = lat;
            this.lng = lng;
        }
        public double lat { get; set; }
        public double lng { get; set; }
    }
}
